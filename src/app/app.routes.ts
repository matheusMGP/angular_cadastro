import { Routes } from '@angular/router';
import { HomeComponent } from './navegacao/home/home.component';
import { SobreComponent } from './institucional/sobre/sobre.component';
import { ContatoComponent } from './institucional/contato/contato.component';
import { DataBindingComponent } from './demos/data-binding/data-binding.component';
import { ListaProdutosComponent } from './produtos/lista-produtos/lista-produtos.component';
import { DetalheProdutoComponent } from './produtos/detalhe-produto/detalhe-produto.component';
import { CarrinhoProdutoComponent } from './produtos/carrinho-produto/carrinho-produto.component';
import { FormsProdutoComponent } from './produtos/forms-produto/forms-produto.component';
import { CadastrarProdutoComponent } from './produtos/cadastrar-produto/cadastrar-produto.component';

export  const rootRouterConfig : Routes = [

    {path: '', redirectTo: '/home', pathMatch: 'full'} ,
    {path: 'home' , component:HomeComponent},
    {path: 'sobre' , component:SobreComponent},
    {path: 'contato' , component:ContatoComponent},
    {path: 'feature-data-binding' , component:DataBindingComponent},
    {path: 'produtos' , component:ListaProdutosComponent},
    {path: 'produto-detalhe/:id' , component:DetalheProdutoComponent},
    {path: 'produto-carrinho/:id' , component:CarrinhoProdutoComponent},
    {path: 'form-produto/:id' , component:FormsProdutoComponent},
    {path: 'cadastrar-produto' , component:CadastrarProdutoComponent},

];