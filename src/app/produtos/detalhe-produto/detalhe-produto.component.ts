import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto';
import { ProdutoService } from '../produtos.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalhe-produto',
  templateUrl: './detalhe-produto.component.html',
  styleUrls: ['./detalhe-produto.component.css']
})
export class DetalheProdutoComponent implements OnInit {

  public produto: Produto;
  constructor(private produtoService: ProdutoService, private route: ActivatedRoute) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.produtoService.obterProdutoById(id)
   .subscribe(
     produto => {
       this.produto = produto;
       console.log(produto);
     },
     error => {
       console.log(error);
     }
   )
  }
  

}
