import { Component, OnInit } from '@angular/core';
import { Produto } from '../produto';
import { ProdutoService } from '../produtos.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-carrinho-produto',
  templateUrl: './carrinho-produto.component.html',
  styleUrls: ['./carrinho-produto.component.css']
})
export class CarrinhoProdutoComponent implements OnInit {

  public produto: Produto;
  constructor(private produtoService: ProdutoService, private route: ActivatedRoute) { }

  ngOnInit() {
    let id = this.route.snapshot.params['id'];
    this.produtoService.obterProdutoById(id)
    .subscribe(
      produto => {
        this.produto = produto;
        console.log(produto);
      },
      error => {
        console.log(error);
      }
    )
  }

}
