import { Component, OnInit,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Produto } from '../produto';
import { ProdutoService } from '../produtos.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastrar-produto',
  templateUrl: './cadastrar-produto.component.html',
  styleUrls: ['./cadastrar-produto.component.css']
})
export class CadastrarProdutoComponent implements OnInit {

  @ViewChild('formProduto', { static: true }) formProduto: NgForm;
  public produto: Produto;

  constructor(private produtoService: ProdutoService, private router: Router, private route: ActivatedRoute) { }



  ngOnInit(): void {
    this.produto = new Produto();
  }
  cadastrar(): void {
    if (this.formProduto.form.valid) {
     alert('sucesso,agora so implementar uma rota post para cadastro')
    }
 }

}
