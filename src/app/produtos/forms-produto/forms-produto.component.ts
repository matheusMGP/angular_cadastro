import { Component, OnInit,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Produto } from '../produto';
import { ProdutoService } from '../produtos.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forms-produto',
  templateUrl: './forms-produto.component.html',
  styleUrls: ['./forms-produto.component.css']
})
export class FormsProdutoComponent implements OnInit {

  @ViewChild('formProduto', { static: true }) formProduto: NgForm;
  public produto: Produto;

  constructor(private produtoService: ProdutoService, private router: Router, private route: ActivatedRoute) { }


  ngOnInit() {
    const id = +this.route.snapshot.params['id'];
    this.produtoService.obterProdutoById(id)
   .subscribe(
     produto => {
       this.produto = produto;
       console.log(produto);
     },
     error => {
       console.log(error);
     }
   )
  }
  atualizar(): void {
    if (this.formProduto.form.valid) {
      //this.tarefaService.atualizar(this.tarefa);
      //this.router.navigate(['/tarefas']);
      alert('valido agora so implementar uma rota put para atualizar no servidor')
    }
  }
  
}
