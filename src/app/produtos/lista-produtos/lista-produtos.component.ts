import { Produto } from './../produto';
import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../produtos.service';


@Component({
  selector: 'app-lista-produtos',
  templateUrl: './lista-produtos.component.html',
  styleUrls: ['./lista-produtos.component.css']
})
export class ListaProdutosComponent implements OnInit {
  
  public produtos: Produto[];
  constructor(private produtoService: ProdutoService) { }

  ngOnInit() {
   this.produtoService.obterProdutos()
   .subscribe(
     produtos => {
       this.produtos = produtos;
       console.log(produtos);
     },
     error => {
       console.log(error);
     }
   )
  }

}
